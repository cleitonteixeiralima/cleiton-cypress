const { cyan } = require("colorette");
const { get } = require("lodash");
const { type } = require("os");

describe ("Tickets", () => {
    beforeEach (() => cy.visit ("https://bit.ly/2XSuwCW"));

    it("Fills all the text input fields" , () => {
        
        const firstName = "Elizabete";
        const lastName = "Lima";

        cy.get ("#first-name").type("Elizabete");
        cy.get ("#last-name").type("Lima");
        cy.get ("#email").type("elizabete08.79@hotmail.com");
        cy.get ("#requests").type("Estudante");
        cy.get ("#signature").type(`${firstName} ${lastName}`);
    });


    it ("Select two tickets" , () => {
        cy.get ("#ticket-quantity").select ("2");

    });

    it ("Select Vip" , () => {
        cy.get ("#vip").check ();

    });

    it("selects 'social media' checkbox" , () => {
        cy.get("#social-media").check();

    });

    it ("Selects 'Friend' and 'publication' then uncheck 'friend' ", () => {
        cy.get ("#friend").check();
        cy.get ("#publication").check();
        cy.get ("#friend").check ();

    });


    it(" has'TICKETBOX' header's heading", () => {
        cy.get ("header h1").should ("contain" , "TICKETBOX")
    });


    it("Alert on Invalid Email", () => {
        cy.get ("#email")
        .as("email")
        .type ("cleitonteixeiralima-gmail.com");

        cy.get("#email.invalid").should ("exist");

        cy.get("#email")
        .clear()
        .type("cleitonteixeiralima@gmail.com");

        cy.get("#email.invalid").should("not.exist");

    });

    it("fills and reset the form", () => {
        const firstName = "Cleiton";
        const lastName = "Lima"
        const fullname = `${firstName} ${lastName}`;

        cy.get ("#first-name").type("Cleiton");
        cy.get ("#last-name").type("Lima");
        cy.get ("#email").type ("bete08.79@gmail.com");
        cy.get ("#ticket-quantity").select("3");
        cy.get ("#vip").check ();
        cy.get ("#friend").check();
        cy.get ("#requests").type("Eu amo musculação");

        cy.get (".agreement p").should("contain",
        `I, ${fullname}, wish to buy 3 VIP tickets. `
        );

        cy.get ("#agree").click();

        cy.get ("#signature").type(fullname);

        cy.get ("button[type= 'submit']")
        .as ("activeButton")
        .should ("not.be.disabled");

        cy.get ("button[type= 'reset']").click();

        cy.get ("@activeButton").should("be.disabled");

    });

    it("fills mandatory fields using support command" ,() => {
        const customer = {
            firstName: "Guilherme",
            lastName: "Lima",
            email: "guita@example.com.br"
        };

        cy.fillMandatoryFields(customer);

        cy.get ("button[type= 'submit']")
        .as ("activeButton")
        .should ("not.be.disabled");

        cy.get ("#agree").uncheck();

        cy.get ("@activeButton").should("be.disabled");
       
    });
});


